package az.feign.test.feigntestwithrestAPI.repository;

import az.feign.test.feigntestwithrestAPI.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
