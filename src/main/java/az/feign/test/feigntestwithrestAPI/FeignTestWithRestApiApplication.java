package az.feign.test.feigntestwithrestAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FeignTestWithRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignTestWithRestApiApplication.class, args);
	}

}
