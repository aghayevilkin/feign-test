package az.feign.test.feigntestwithrestAPI.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "example-service", url = "https://newsapi.org")
public interface ArticleFeignClient {
    @GetMapping("/v2/top-headlines?sources=techcrunch&apiKey=4274c423578140e6be879d6a69a1bca3")
    Map<String, Object> getResource();
}