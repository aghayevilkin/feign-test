package az.feign.test.feigntestwithrestAPI.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().info(createApiInfo());
    }

    private Info createApiInfo() {
        return new Info()
                .title("ExpressBank Task API")
                .description("This is the API documentation for 'Express Bank Task' application.")
                .version("1.0");
    }
}