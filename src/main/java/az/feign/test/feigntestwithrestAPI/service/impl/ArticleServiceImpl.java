package az.feign.test.feigntestwithrestAPI.service.impl;

import az.feign.test.feigntestwithrestAPI.client.ArticleFeignClient;
import az.feign.test.feigntestwithrestAPI.domain.Article;
import az.feign.test.feigntestwithrestAPI.domain.Source;
import az.feign.test.feigntestwithrestAPI.dto.response.ArticleResponseDto;
import az.feign.test.feigntestwithrestAPI.repository.ArticleRepository;
import az.feign.test.feigntestwithrestAPI.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;
    private final ArticleFeignClient client;
    private final ModelMapper mapper;

    @Override
    public ArticleResponseDto articleInfo() {
        Map<String, Object> clientResource = client.getResource();
        List<Map<String, Object>> articles = (List<Map<String, Object>>) clientResource.get("articles");

        List<Article> articleList = new ArrayList<>();
        for (Map<String, Object> articleObject : articles) {
            Object sourceObject = articleObject.get("source");
            Article article = mapper.map(articleObject, Article.class);
            Source source = mapper.map(sourceObject, Source.class);

            article.setSource(source);
            articleList.add(article);
        }
        articleRepository.saveAll(articleList);
        return ArticleResponseDto.builder().data(clientResource).build();
    }
}
