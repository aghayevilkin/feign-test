package az.feign.test.feigntestwithrestAPI.service;

import az.feign.test.feigntestwithrestAPI.dto.response.ArticleResponseDto;

public interface ArticleService {
    ArticleResponseDto articleInfo();
}
