package az.feign.test.feigntestwithrestAPI.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class BaseResponse<T> {

    private HttpStatus status;
    private int totalResult;
    private T data;

    public BaseResponse(HttpStatus status, int totalResult, T data) {
        this.status = status;
        this.totalResult = totalResult;
        this.data = data;
    }
}
