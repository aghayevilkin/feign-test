package az.feign.test.feigntestwithrestAPI.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class ArticleResponseDto {

    private Map<String, Object> data;
}
