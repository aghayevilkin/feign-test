package az.feign.test.feigntestwithrestAPI.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "sources")
public class Source {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long base_id;

    @Column(name = "client_id")
    private String id;

    @Column(name = "name")
    private String name;
}
