package az.feign.test.feigntestwithrestAPI.controller;

import az.feign.test.feigntestwithrestAPI.dto.BaseResponse;
import az.feign.test.feigntestwithrestAPI.dto.response.ArticleResponseDto;
import az.feign.test.feigntestwithrestAPI.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/article")
@RequiredArgsConstructor
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping("/article-info")
    public ArticleResponseDto articleInfo() {
        return articleService.articleInfo();
    }
}
